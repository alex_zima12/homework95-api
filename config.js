const path = require("path");

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, "public/uploads"),
  db: {
    name: "CocktailApp",
    url: "mongodb://localhost"
  },
  fb: {
    appId: "248618003298247",
    appSecret:"11459a19dfb99989f5c527b285cdd5df"
  }
};

