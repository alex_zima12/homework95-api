const router = require("express").Router();
const multer = require('multer');
const {nanoid} = require('nanoid');
const path = require('path');
const config = require('../config');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const User = require('../models/User');
const Cocktail = require('../models/Cocktail');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});


router.get('/', async (req, res) => {
    const token = req.get('Authorization');
    if (token) {
        req.user = await User.findOne({token});
    }
    const criteria = {isPublished: true};

    if (req.user && req.user.role === 'admin') {
        delete criteria.isPublished
    }

    if (req.query.user) {
        delete criteria.isPublished;
        criteria.user = req.query.user;
    }

    try {
        const cocktails = await Cocktail.find(criteria);
        return res.send(cocktails);
    } catch {
        return res.sendStatus(500);
    }
});

router.get('/:id', async (req, res) => {
    const token = req.get('Authorization');
    if (token) {
        req.user = await User.findOne({token});
    }
    const criteria = {isPublished: true, _id: req.params.id};

    if (req.user) {
        delete criteria.isPublished;
        criteria.user = req.user  
        
        if (req.user.role === 'admin') {
            delete criteria.user;
        }
    } 

    try {
        const cocktail = await Cocktail.findOne(criteria);

        if (cocktail) {
            return res.send(cocktail);
        } else {
            return res.status(404).send({message: 'Cocktail not found!'});
        }

    } catch {
        return res.status(404).send({message: 'Cocktail not found!'});
    }
});

router.post('/', auth, upload.single('image'), async (req, res) => {

    const cocktailData = {
        name: req.body.name,
        ingredients: JSON.parse(req.body.ingredients),
        recipe: req.body.recipe,
        user: req.user._id
    };
    
    if (req.file) {
        cocktailData.image = req.file.filename;
    }

    try {
        const cocktail = new Cocktail(cocktailData);

        await cocktail.save();
        return res.send({message: 'Cocktail added!', cocktail});
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    try {
        await Cocktail.deleteOne({_id: req.params.id});
        return res.status(200).send({message: 'Cocktail deleted!'});
    } catch {
        return res.status(400);
    }
});

router.put('/:id/publish', [auth, permit('admin')], async (req, res) => {

    try {
        const cocktail = await Cocktail.findById(req.params.id);

        if(!cocktail) {
            return res.sendStatus(404);
        }
        console.log(cocktail);
        cocktail.isPublished = true;
        await cocktail.save();
        return res.send(cocktail);
    } catch {
        return res.sendStatus(400);
    }
});

module.exports = router;